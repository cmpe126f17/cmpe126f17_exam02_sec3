# Lab Exam 2 #
In this exam you will be implementing an ordered doubly linked list either. In addition to the usual insert() and delete() methods you will also implement union() and intersection() methods. Ordered linked lists are great for getting max or min values in O(1) time. Hence, you will also write print_topk() and print_bottomk() methods.

The exam is open everything but this does not mean that you can copy paste a solution from an internet resource. Dont forget copy/paste is plagiarism and is not appropriate by SJSU. You may go read learn how to do it but code it by your own. This is completely fine.
