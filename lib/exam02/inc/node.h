#ifndef OREDERD_DOUBLYLIST_NODE_H
#define OREDERD_DOUBLYLIST_NODE_H


class node {
public:
    node* prev;
    node* next;
    int data;

    node();
    node(int input);
    ~node();

};

#endif //OREDERD_DOUBLYLIST_NODE_H
