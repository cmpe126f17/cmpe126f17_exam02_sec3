#ifndef ORDERED_DOUBLYLIST_H
#define ORDERED_DOUBLYLIST_H

#include "node.h"

class ordered_doubly_list
{
private:
    node* head;
    node* tail;
    int size;

public:
    ordered_doubly_list();
    ordered_doubly_list(int value);
    ordered_doubly_list(int values[], int length);
    ~ordered_doubly_list();

    void insert(int value);
    void remove(int value);

    void list_intersect(ordered_doubly_list* list_to_intersect);
    void list_union(ordered_doubly_list* list_to_union);

    bool get_size();

    int get_value(int location);

    void print();
    void print_topk();
    void print_bottomk();

};


#endif //ORDERED_DOUBLYLIST_H
