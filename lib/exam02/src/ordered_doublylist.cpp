#include "ordered_doublylist.h"
#include <iostream>

// Default constructor for creating a doubly linked list with nothing in it
ordered_doubly_list::ordered_doubly_list()
{
    head = nullptr;
}

// Default constructor for creating a doubly linked list with a given value
ordered_doubly_list::ordered_doubly_list(int value)
{

}

// Default constructor for creating an ordered doubly linked list from a given integer array
ordered_doubly_list::ordered_doubly_list(int values[], int length)
{

}

// Default destructor. Should run through each of the nodes and delete them
ordered_doubly_list::~ordered_doubly_list()
{

}

// Add a single value to the ordered doubly linked list
void ordered_doubly_list::insert(int value)
{

}

// Remove a value if exists
void ordered_doubly_list::remove(int value) {

}


// Intersect the ordered doubly linked list with a given ordered doubly linked list
void ordered_doubly_list::list_intersect(ordered_doubly_list* list_to_intersect)
{
   
}

// Union the ordered doubly linked list with a given the ordered doubly linked list
void ordered_doubly_list::list_union(ordered_doubly_list* list_to_union)
{
   
}



// Get the size of the ordered doubly linked list
bool ordered_doubly_list::get_size()
{


}


// Get the data at the given location
int ordered_doubly_list::get_value(int location)
{

}

// Print top k values
void ordered_doubly_list::print_topk()
{


}

// Print bottom k values
void ordered_doubly_list::print_bottomk()
{


}

// Print the entire linked list
void ordered_doubly_list::print()
{

}
